#!/usr/bin/env python
# coding: utf-8

# In[4]:


# -*- coding: utf-8 -*-
import json
import pandas as pd
import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.express as px
import plotly.graph_objs as go


# In[3]:


filename = r'D:\graph2.json'
with open(filename, "r", encoding = 'utf-8') as f:  
    data = f.read()  
d = json.loads(data)
df1 = pd.DataFrame(d['node'])
df2 = pd.DataFrame(d['edge'])


lstx = []
for i in range(len(df2)):
    lstx.append(df2['x1'][i])
    lstx.append(df2['x2'][i])
    lstx.append(None)
lsty = []
for i in range(len(df2)):
    lsty.append(df2['y1'][i])
    lsty.append(df2['y2'][i])
    lsty.append(None)
lstz = []
for i in range(len(df2)):
    lstz.append(df2['z1'][i])
    lstz.append(df2['z2'][i])
    lstz.append(None)
    
    
hover_text = []
for index, row in df1.iterrows():
    hover_text.append(('node_name: {node_name}<br>'+
                      'author: {author}<br>'+
                      'url: {url}').format(node_name=row['node_name'],
                                            author=row['author'],
                                            url=row['url']))
df1['text'] = hover_text


# In[ ]:


app = dash.Dash()

fig = go.Figure() 

fig.add_trace(go.Scatter3d(x=lstx,
               y=lsty,
               z=lstz,
               mode='lines',
               line=dict(color='rgb(125,125,125)', width=2),
               hoverinfo='none'
               ))

fig.add_trace(go.Scatter3d(x=df1.x,
               y=df1.y,
               z=df1.z,
               mode='markers',
               marker=dict(symbol='circle',
                            size=df1.size_reference,
                            color=df1['color'],
                             ),
               text= df1['text']
               ))


axis=dict(showbackground=False,
          showline=False,
          zeroline=False,
          showgrid=False,
          showticklabels=False,
          title=''
          )


fig.update_layout(title="3D Network",
         width=800,
         height=800,
         showlegend=False,
         hovermode='closest',
         scene=dict(
             xaxis=dict(axis),
             yaxis=dict(axis),
             zaxis=dict(axis)
        ))


fig2 = go.Figure() 

fig2.add_trace(go.Scatter(x=lstx,
               y=lsty,
               mode='lines',
               line=dict(color='rgb(125,125,125)', width=2),
               hoverinfo='none'
               ))

fig2.add_trace(go.Scatter(x=df1.x,
               y=df1.y,
               mode='markers',
               marker=dict(symbol='circle',
                            size=df1.size_reference,
                            color=df1['color'],
                             ),
               text= df1['text']
               ))


fig2.update_layout(title="2D Network",
         width=800,
         height=800,
         showlegend=False,
         hovermode='closest',
         xaxis=dict(showgrid=False, zeroline=False, showticklabels=False),
         yaxis=dict(showgrid=False, zeroline=False, showticklabels=False)
                 )


app.layout = html.Div(children=[
    html.H2(children='3D Network with Dash'),

    dcc.Graph(
        id='3D',
        figure=fig
    ),
    
    html.H2(children='2D Network with Dash'),
    dcc.Graph(figure=fig2)
])

if __name__ == '__main__':
    app.run_server(debug=True)

