#!/usr/bin/env python
# coding: utf-8

# In[1]:


import dash
import dash_core_components as dcc
import dash_html_components as html
import math
import pandas as pd
import plotly.express as px
import plotly.graph_objs as go


# In[ ]:


df = px.data.iris() # iris is a pandas DataFrame
fig = px.scatter(df, x="sepal_width", y="sepal_length")

app = dash.Dash()

app.layout = html.Div([
    html.H2(children='Iris with Dash'),
    dcc.Graph(figure=fig)
]
)

if __name__ == '__main__':
    app.run_server(debug=True)

